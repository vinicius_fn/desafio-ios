//
//  ShowPullRequestViewControllerTests.swift
//  ConcreteGitHubTests
//
//  Created by Vinicius França on 05/11/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import XCTest
@testable import ConcreteGitHub

class ShowPullRequestViewControllerTests: XCTestCase {
    
    var showPullRequestDataSource : ShowPullRequestDataSource?
    var showPullRequestDelegate : ShowPullRequestDelegate?
    var fakeTableView : UITableView = UITableView()
    var pulls : [Pull] = []
    var controller : ShowPullRequestViewController!
    
    override func setUp() {
        super.setUp()
        let pull = Pull(id: 1, title: "Teste", body: "Teste Body", htmlURL: URL(string: "wwww.google.com.br")!, state: "", createdAt: "", user: User(id: 1, login: "teste", avatarURL: URL(string: "www.google.com.br")!))
        self.pulls.append(pull)
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        self.controller = storyboard.instantiateViewController(withIdentifier: "showPullRequestViewControllerId") as! ShowPullRequestViewController
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSetupTableView() {
        self.showPullRequestDelegate = ShowPullRequestDelegate(self.controller.self)
        self.showPullRequestDataSource = ShowPullRequestDataSource(pulls: self.pulls, tableView: self.fakeTableView, delegate: self.showPullRequestDelegate!)
        XCTAssertNotNil(self.showPullRequestDelegate)
        XCTAssertNotNil(self.showPullRequestDataSource)
        XCTAssertEqual(self.pulls.count, 1)
        
    }
    
}
