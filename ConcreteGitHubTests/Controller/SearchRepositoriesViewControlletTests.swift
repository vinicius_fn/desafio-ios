//
//  SearchRepositoriesViewControlletTests.swift
//  ConcreteGitHubTests
//
//  Created by Vinicius França on 04/11/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import XCTest
@testable import ConcreteGitHub

class SearchRepositoriesViewControlletTests: XCTestCase {
    
    var searchRepositoriesDataSource : SearchRepositoriesDataSource?
    var searchRepositoriesDelegate : SearchRepositoriesDelegate?
    var items : [Item] = []
    var fakeTableView : UITableView = UITableView(frame: CGRect.zero)
    var controller : SearchRepositoriesViewController!
    
    override func setUp() {
        super.setUp()
        let item : Item = Item(id: 1, name: "Teste", fullName: "Full Test", htmlURL: URL(string: "http://wwww.google.com.br")!, desc: "descricao", forksCount: 111, stargazersCount: 111, owner: Owner(id: 1, login: "concrete", avatarURL: URL(string: "http://wwww.google.com.br")!))
        self.items.append(item)
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        self.controller = storyboard.instantiateViewController(withIdentifier: "searchRepositoriesViewControllerId") as! SearchRepositoriesViewController
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testSetupTableView() {
        self.searchRepositoriesDelegate = SearchRepositoriesDelegate(self.controller.self, tableView: fakeTableView)
        self.searchRepositoriesDataSource = SearchRepositoriesDataSource(items: self.items, tableView: self.fakeTableView, delegate: self.searchRepositoriesDelegate!)
        XCTAssertNotNil(self.searchRepositoriesDelegate)
        XCTAssertNotNil(self.searchRepositoriesDataSource)
        XCTAssertEqual(self.items.count, 1)
        
    }
    
    
    
}
