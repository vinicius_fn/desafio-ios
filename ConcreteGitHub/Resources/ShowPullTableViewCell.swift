//
//  ShowPullTableViewCell.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 30/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import UIKit
import Reusable
import AlamofireImage

class ShowPullTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var titlePullLabel: UILabel!
    @IBOutlet weak var descriptionPullLabel: UILabel!
    @IBOutlet weak var datePullLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(pull : Pull) {
        self.titlePullLabel.text = pull.title
        self.descriptionPullLabel.text = pull.body
        self.datePullLabel.text = "\(pull.createdAt)"
        self.userNameLabel.text = pull.user.login
        self.userImageView.af_setImage(withURL: pull.user.avatarURL)
    }
    
    static func height () -> CGFloat {
        return 110
    }
    
}
