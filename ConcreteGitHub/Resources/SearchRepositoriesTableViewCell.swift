//
//  SearchRepositoriesTableViewCell.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import UIKit
import Reusable
import AlamofireImage

class SearchRepositoriesTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var nameRepositoryLabel: UILabel!
    @IBOutlet weak var descriptionRepositoryLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userNameFullLabel: UILabel!
    @IBOutlet weak var forksNumberLabel: UILabel!
    @IBOutlet weak var starsNumberLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userNameFullLabel.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell (item : Item) {
        self.nameRepositoryLabel.text = item.name
        self.descriptionRepositoryLabel.text = item.desc
        self.userNameLabel.text = item.owner.login
        self.userNameFullLabel.text = item.fullName
        self.forksNumberLabel.text = "\(item.forksCount)"
        self.starsNumberLabel.text = "\(item.stargazersCount)"
        self.userImageView.image = nil
        self.userImageView.af_setImage(withURL: item.owner.avatarURL)
    }
    
    static func height () -> CGFloat {
        return 100
    }
    
}
