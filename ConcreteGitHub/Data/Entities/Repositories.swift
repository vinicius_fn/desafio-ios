//
//  Repositories.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation

struct Repositories : Decodable {
    
    let items : [Item]
    
}
