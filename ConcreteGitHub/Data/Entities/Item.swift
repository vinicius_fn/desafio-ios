//
//  Item.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation

struct Item : Decodable {
    
    let id : Int
    let name : String
    let fullName : String
    let htmlURL : URL
    let desc : String?
    let forksCount : Int
    let stargazersCount : Int
    let owner : Owner
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case fullName = "full_name"
        case htmlURL = "html_url"
        case desc = "description"
        case forksCount = "forks_count"
        case stargazersCount = "stargazers_count"
        case owner
    }
    
}
