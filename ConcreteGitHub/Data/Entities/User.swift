//
//  User.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation

struct User : Decodable {
    
    let id : Int
    let login : String
    let avatarURL : URL
    
    enum CodingKeys : String, CodingKey {
        case id
        case login
        case avatarURL = "avatar_url"
    }
    
}
