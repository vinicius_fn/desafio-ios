//
//  Pull.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation

struct Pull : Decodable {
    
    let id : Int
    let title : String
    let body : String
    let htmlURL : URL
    let state : String
    let createdAt : String
    let user : User
    
    enum CodingKeys : String, CodingKey {
        case id
        case title
        case body
        case htmlURL = "html_url"
        case state
        case createdAt = "created_at"
        case user
    }
    
}
