//
//  GitHubAPI.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation
import Alamofire

enum Result<T> {
    case success(T)
    case error(Error)
}

struct GenericError : Error {
    
}

class GitHubAPI {
    
    class func searchRepositories(page: Int = 1, _ completion: @escaping (Result<Repositories>) -> Void) {
        
        let url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        let encoding = JSONEncoding.default
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: encoding, headers: nil).responseData { response in
            
            guard let data = response.data else {
                completion(.error(GenericError()))
                return
            }
            
            do {
                let responseItem = try JSONDecoder().decode(Repositories.self, from: data)
                completion(.success(responseItem))
            } catch {
                completion(.error(response.error ?? error))
            }
            
        }
        
    }
    
    class func showPullRequest(create : String, repository : String, _ completion: @escaping (Result<[Pull]>) -> Void) {
        
        let url = "https://api.github.com/repos/\(create)/\(repository)/pulls"
        let encoding = JSONEncoding.default
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: encoding, headers: nil).responseData { response in
            
            guard let data = response.data else {
                completion(.error(GenericError()))
                return
            }
            
            do  {
                let responsePull = try JSONDecoder().decode([Pull].self, from: data)
                completion(.success(responsePull))
            } catch {
                completion(.error(response.error ?? error))
            }
            
        }
        
    }
    
}
