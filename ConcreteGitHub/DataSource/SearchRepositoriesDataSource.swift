//
//  SearchRepositoriesDataSource.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation
import UIKit

class SearchRepositoriesDataSource : NSObject, UITableViewDataSource {
    
    var items : [Item] = []
    weak var tableView : UITableView?
    weak var delegate : UITableViewDelegate?
    
    required init(items: [Item], tableView : UITableView, delegate : UITableViewDelegate) {
        self.items = items
        self.tableView = tableView
        self.delegate = delegate
        super.init()
        tableView.register(cellType: SearchRepositoriesTableViewCell.self)
        self.setupTable()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SearchRepositoriesTableViewCell.self)
        let item = self.items[indexPath.row]
        cell.setupCell(item: item)
        return cell
    }
    
    func setupTable() {
        self.tableView?.dataSource = self
        self.tableView?.delegate = self.delegate
        self.tableView?.reloadData()
    }
    
    func reloadItems(with items : [Item]) {
        self.items.append(contentsOf: items)
        self.tableView?.reloadData()
    }
    
}
