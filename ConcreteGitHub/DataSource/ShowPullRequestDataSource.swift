//
//  ShowPullRequestDataSource.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 30/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation
import UIKit

final class ShowPullRequestDataSource : NSObject, UITableViewDataSource {
    
    var pulls : [Pull] = []
    weak var tableView : UITableView?
    weak var delegate : UITableViewDelegate?
    
    required init(pulls : [Pull], tableView : UITableView, delegate : UITableViewDelegate) {
        self.pulls = pulls
        self.tableView = tableView
        self.delegate = delegate
        super.init()
        tableView.register(cellType: ShowPullTableViewCell.self)
        self.setupTable()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pulls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ShowPullTableViewCell.self)
        let pull = self.pulls[indexPath.row]
        cell.setupCell(pull: pull)
        return cell
    }
    
    func setupTable() {
        self.tableView?.dataSource = self
        self.tableView?.delegate = self.delegate
        self.tableView?.reloadData()
    }
    
}
