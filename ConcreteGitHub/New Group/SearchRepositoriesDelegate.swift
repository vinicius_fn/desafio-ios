//
//  SearchRepositoriesDelegate.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation
import UIKit

protocol SearchRepositoryDelegate {
    func didSelected (indexPath : IndexPath)
    func nextPage(page : Int)
    func refreshControl(refresh : UIRefreshControl)
}

class SearchRepositoriesDelegate : NSObject, UITableViewDelegate {
    
    var pagination : Int = 1
    let delegate : SearchRepositoryDelegate
    let tableView : UITableView
    let refreshControll : UIRefreshControl = UIRefreshControl(frame: .zero)
    
    init( _ delegate : SearchRepositoryDelegate, tableView : UITableView) {
        self.delegate = delegate
        self.tableView = tableView
        super.init()
        self.setupRefreshControll()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SearchRepositoriesTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.didSelected(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            
            let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
            loadingIndicator.color = UIColor.black
            loadingIndicator.startAnimating()
            loadingIndicator.frame = CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: 44.0)
            
            tableView.tableFooterView = loadingIndicator
            tableView.tableFooterView?.isHidden = false
            
            self.pagination += 1
            self.delegate.nextPage(page: self.pagination)
            
        }
    }
    
    func setupRefreshControll() {
        self.refreshControll.addTarget(self, action: #selector(refreshControllItems), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControll
        } else {
            self.tableView.addSubview(self.refreshControll)
        }
    }
    
    @objc func refreshControllItems() {
        self.delegate.refreshControl(refresh: self.refreshControll)
    }
    
}
