//
//  ShowPullRequestDelegate.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 30/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import Foundation
import UIKit

protocol ShowPullDelegate {
    func didSelected(indexPath : IndexPath)
}

class ShowPullRequestDelegate : NSObject, UITableViewDelegate {
    
    let delegate : ShowPullDelegate
    
    init( _ delegate : ShowPullDelegate) {
        self.delegate = delegate
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ShowPullTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.didSelected(indexPath: indexPath)
    }
}
