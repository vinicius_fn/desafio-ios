//
//  SearchRepositoriesViewController.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import UIKit
import Reusable

final class SearchRepositoriesViewController: UIViewController, SearchRepositoryDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var searchRepositoriesDataSource: SearchRepositoriesDataSource?
    var searchRepositoriesDelegate : SearchRepositoriesDelegate?
    
    var items : [Item] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchItems()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pullRequestSegue" {
            let showPullRequestViewController = segue.destination as! ShowPullRequestViewController
            showPullRequestViewController.item = sender as! Item
        }
        
    }
    
    func fetchItems() {
        GitHubAPI.searchRepositories() { result in
            switch result {
            case .success(let repositories):
                self.setupTableView(with: repositories.items)
            case .error(_):
                
                let messageAlertController = UIAlertController(title: "Erro de conexão", message: "Não foi possivel realizar o carregamento.", preferredStyle: .alert)
                let retryAction = UIAlertAction(title: "Tentar Novamente", style: .default, handler: { ( _ ) in
                    self.fetchItems()
                })
                let cancelAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)

                messageAlertController.addAction(retryAction)
                messageAlertController.addAction(cancelAction)

                DispatchQueue.main.async {
                   self.present(messageAlertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func fetchMoreItems(page : Int) {
        GitHubAPI.searchRepositories(page: page) { result in
            switch result {
            case .success(let repositories):
                self.refreshItemsTableView(with: repositories.items)
            case .error(_):
                
                let messageAlertController = UIAlertController(title: "Erro de conexão", message: "Não foi possivel realizar o carregamento.", preferredStyle: .alert)
                let retryAction = UIAlertAction(title: "Tentar Novamente", style: .default, handler: { ( _ ) in
                    self.fetchMoreItems(page: page)
                })
                let cancelAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)

                messageAlertController.addAction(retryAction)
                messageAlertController.addAction(cancelAction)

                DispatchQueue.main.async {
                    self.present(messageAlertController, animated: true, completion: nil)
                }
                
            }
        }
    }

    func setupTableView(with items : [Item]) {
        self.items = items
        self.searchRepositoriesDelegate = SearchRepositoriesDelegate(self, tableView: self.tableView)
        self.searchRepositoriesDataSource = SearchRepositoriesDataSource(items: self.items, tableView: self.tableView, delegate: self.searchRepositoriesDelegate!)
    }
    
    func refreshItemsTableView(with items : [Item]) {
        self.searchRepositoriesDataSource?.reloadItems(with: items)
    }
    
    func didSelected(indexPath: IndexPath) {
        self.performSegue(withIdentifier: "pullRequestSegue", sender: self.items[indexPath.row])
    }
    
    func nextPage(page: Int) {
        self.fetchMoreItems(page: page)
    }
    
    func refreshControl(refresh: UIRefreshControl) {
        self.fetchItems()
    }
}


