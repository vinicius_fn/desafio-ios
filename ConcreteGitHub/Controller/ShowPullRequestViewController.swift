//
//  ShowPullRequestViewController.swift
//  ConcreteGitHub
//
//  Created by Vinicius França on 29/10/2017.
//  Copyright © 2017 Vinicius França. All rights reserved.
//

import UIKit
import SafariServices

class ShowPullRequestViewController: UIViewController, ShowPullDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var showPullRequestDataSource: ShowPullRequestDataSource?
    var showPullRequestDelegate : ShowPullRequestDelegate?
    
    var item : Item!
    var pulls : [Pull] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.item.name
        self.fetchPulls(with: item.owner.login, repository: item.name)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func fetchPulls(with create : String, repository : String) {
        GitHubAPI.showPullRequest(create: create, repository: repository) { result in
            switch result {
            case .success(let pulls):
                self.setupTableView(with: pulls)
            case .error(let error):
                print(error)
            }
        }
    }
    
    func setupTableView(with pulls : [Pull]) {
        self.pulls = pulls
        self.showPullRequestDelegate = ShowPullRequestDelegate(self)
        self.showPullRequestDataSource = ShowPullRequestDataSource(pulls: self.pulls, tableView: self.tableView, delegate: self.showPullRequestDelegate!)
    }
    
    func didSelected(indexPath: IndexPath) {
        
        if #available(iOS 9.0, *) {
            let safariViewController = SFSafariViewController(url: self.pulls[indexPath.row].htmlURL)
            self.present(safariViewController, animated: true, completion: nil)
        } else {
            UIApplication.shared.openURL(self.pulls[indexPath.row].htmlURL)
        }
        
        
    }

}
